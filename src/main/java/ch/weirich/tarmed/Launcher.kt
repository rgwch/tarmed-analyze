/*
 * Copyright (c) 2016 by G. Weirich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package ch.weirich.tarmed

import java.io.File
import java.io.FileInputStream
import java.io.InputStream

/**
 * Der Programmstarter. Nimmt ein oder zwei Parameter als Argument. Das erste ist das Rechnungsverzeichnis, das zweite
 * eine Konfigurationsdatei. Wenn das zweite Argument fehlt, wird die eingebaute Konfigurationsdatei (transript.cfg)
 * übernommen.
 */

fun main(args: Array<String>){

    var cfgFile: InputStream?=null

    if(args.size<1){
        System.out.println("usage: java -jar tarmed-analyze.jar <verzeichnis mit Rechnungen> [<Transkriptionsanweisungen>]")
        System.exit(-1)
    }
    if(args.size==2){
        val file=File(args[1])
        if(file.exists() && file.canRead()){
            cfgFile=FileInputStream(file)
        }else{
            println("Kann Konfigurationsdatei (${args[1]} nicht öffnen. (Aus Verzeichnis ${File(".").absolutePath})\nVerwende default.")
        }
    }
    val resultText=StringBuilder()
    val tr=Transcript()
    val dir= File(args[0])
    // Rechnungen einlesen
    val bills=Loader().addDirectory(dir,resultText)
    println(resultText.toString())
    var oldValue=0.0
    var newValue=0.0
    // Jede Rechnung einzeln umrechnen.
    bills.forEach {
        println("\nDatum: "+it.date)
        val oldsum=it.tarmedpos.fold(0.0) { sum,tr -> sum+tr.value }
        println("\nTarmed: "+it.tarmedpos.joinToString { it.id }+" - Summe: ${round100(oldsum)}")
        resultText.setLength(0)
        val output=tr.translateCons(it,resultText)
        val newsum=output.fold(0.0){sum,px -> sum+(px.AL+px.TL)*tr.tp}
        println("\nPhoenix: "+output.joinToString{ it.getCode() }+" - Summe: ${round100(newsum)}")
        println("-------------------------")
        oldValue+=oldsum
        newValue+=newsum
    }
    println("\nTotalsumme alt: ${round100(oldValue)}, neu: ${round100(newValue)}")
}