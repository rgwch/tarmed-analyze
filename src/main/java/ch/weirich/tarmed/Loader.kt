/*
 * Copyright (c) 2016 by G. Weirich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package ch.weirich.tarmed

import org.jdom2.Element
import org.jdom2.Namespace
import org.jdom2.filter.Filters
import org.jdom2.input.SAXBuilder
import org.jdom2.xpath.XPathFactory
import java.io.File
import java.io.FilenameFilter
import java.util.*

/**
 * Rechnungen einlesen und in Tarmedpositionen aufsplitten.
 */
class Loader {
    val sax = SAXBuilder()
    val conses = LinkedList<Consultation>()
    val ns = arrayListOf(Namespace.getNamespace("invoice", "http://www.xmlData.ch/xmlInvoice/XSD"))
    val vars = mapOf<String, Object>()

    /**
     * Verzeichnis mit Rechnungen hinzufügen (Beliebig viele sind möglich)
     */
    fun addDirectory(dir: File, resultText:StringBuilder):List<Consultation>{
        if (!dir.exists() || !dir.isDirectory()) {
            throw Exception("Bitte als ersten Parameter ein Verzeichnis mit XML Rechnungen angeben.")
        }
        val files = dir.listFiles(FilenameFilter { dir, name -> name.endsWith(".xml") })
        files.forEach {
            val doc = sax.build(it)
            val xf = XPathFactory.instance()
            val tarmedrecord = xf.compile("//invoice:record_tarmed", Filters.element(), vars, ns)
            val records = tarmedrecord.evaluate(doc)
            val dates = records.groupBy { element -> element.getAttributeValue("date_begin") }
            dates.forEach { date ->
                val cons = Consultation(date.key, makeCons(date.value))
                conses.push(cons)
            }
        }
        resultText.append("${conses.size} Konsultationen in ${files.size} Rechnungsdateien wurden eingelesen.\n")
        return conses
    }

    /**
     * Konsultationen erstellen. Alle Tarifpositionen mit demselben Datum werden zusammengefasst.
     *
     */
    // TODO: mehrere Konsultationen am selben Tag werden nicht unterschieden
    private fun makeCons(elements: List<Element>): List<Tarmedpos> {
        val ret = ArrayList<Tarmedpos>()
        elements.forEach { el ->
            var codeclass=el.getAttributeValue("tariff_type")
            // Nur Tariftyp "Tarmed" übernehmen
            if(codeclass=="001"){
                var num = el.getAttributeValue("quantity").toDouble()
                while (num-- > 0) {
                    val al = el.getAttributeValue("unit.mt").toDouble()
                    val tp = el.getAttributeValue("unit_factor.mt").toDouble()
                    val tl = el.getAttributeValue("unit.tt").toDouble()
                    val value = round100(al * tp + tl * tp)
                    ret.add(Tarmedpos(el.getAttributeValue("code"), tl, al, value))
                }
            }
        }
        return ret
    }

}

/** Rein pekuniärer Blick auf eine Konsultation: Ein Datum und eine Liste von Tarmedpositionen. */
data class Consultation(val date: String, val tarmedpos: List<Tarmedpos>)
/**
 *Vereinfachte Tarmedposition: Taxpunkte für TL und AL, sowie Wert der aus TL+AL, multipliziert mit
 * dem Taxpunktwert in der Originalposition besteht.
 */
data class Tarmedpos(val id: String, val tptl: Double, val tpal: Double, val value: Double)

fun round100(amount: Double):Double{
    return Math.round(amount*100)/100.0
}