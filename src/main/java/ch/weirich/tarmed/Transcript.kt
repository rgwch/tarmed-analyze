/*
 * Copyright (c) 2016 by G. Weirich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package ch.weirich.tarmed

import java.io.InputStream
import java.io.InputStreamReader
import java.util.*
import java.util.logging.Logger

/**
 * Created by gerry on 03.04.16.
 */
class Transcript(val inp: InputStream? = null) {
    val log = Logger.getLogger("Transcript")
    val rules = LinkedList<Rule>()
    val phoenixList by lazy {
        val liste = InputStreamReader(ClassLoader.getSystemClassLoader().getResourceAsStream("listecomplet.txt"))
        liste.readLines().filter { it.endsWith("D|") }.map { Phoenix(it) }
    }
    val phoenixData = TreeMap<String, Phoenix>()
    var tp = 0.82


    init {
        loadTranscript(inp)
    }

    /**
     * Regelsatz für die Transkription einlesen
     */
    fun loadTranscript(inp: InputStream? = null) {
        val reader = if (inp == null) {
            InputStreamReader(ClassLoader.getSystemClassLoader().getResourceAsStream("transcript.cfg"))
        } else {
            InputStreamReader(inp)
        }
        val raw = reader.readText()
        // Kommentare, Whitespace und linebreaks entfernen
        val nocomment = raw.replace("#.*$".toRegex(RegexOption.MULTILINE), "")
        val cooked = nocomment.replace("[\\s\\n\\r\\t]+".toRegex(), " ")
        val regex = Regex("\\{(.+?)\\}")
        val results = regex.findAll(cooked)
        // Einzelne Regel analysieren
        results.forEach {
            val line = it.value.replace("[\\{\\}]".toRegex(), "").split("\\s*to\\s*".toRegex())
            if (line.size == 2) {
                val oldVals = line[0].trim().split(" ".toRegex()).toList()
                val newVals = ArrayList<Transcription>()
                line[1].replace(" +".toRegex(), "").split("\\|".toRegex()).forEach { px ->
                    val lr = px.split(":")
                    val percent = if (lr.size == 2) {
                        lr[1].toDouble()
                    } else {
                        1.0
                    }
                    newVals.add(Transcription(lr[0], percent))
                }
                rules.add(Rule(oldVals, newVals))
            } else {
                // Keine Regel, sondern eine globale Variable
                val global = line[0].replace(" *".toRegex(), "").split(":".toRegex())
                if (global[0] == "tp") {
                    tp = global[1].toDouble()
                }
            }
        }
        reader.close()
    }

    /**
     * Vergleiche alle Rules mit einer Konsultation.
     * Eine Rule kommt in Frage, wenn sie alle oder einige der Positionen der
     * Konsultation enthält. Eine Rule kommt nicht in Frage, wenn sie Positionen
     * enthält, die in der Konsultation nicht vorkommen.
     * Wenn eine Rule in Frage kommt, dann passt sie um so besser, je mehr Positionen
     * der Konsultation sie enthält, also je länger sie ist. Wenn die Konsultation nur die
     * Positionen der Rule enthält, also gleich lang ist, dann gibt es einen extra-score.
     * Diese Methode liefert die so errechnete beste Rule zurück.
     */
    fun findMatchingRule(fromCons: List<String>): Rule {
        log.finest("Finding match for ${fromCons}")
        // als fallback/default ist die letzte Rule des Rulesets vorgegeben, mit einem Score von 0: Jede andere
        // akzeptable Rule wird besser sein.
        var bestMatch = rules.last()
        var score = 0
        fun isAcceptable(rule: Rule): Boolean {
            val conslist = ArrayList(fromCons)
            rule.old.forEach {
                if (!conslist.remove(it)) {
                    return false;
                }
            }
            return true
        }

        rules.forEach {
            if (isAcceptable(it)) {
                val localScore = if (it.old.size == fromCons.size) {
                    it.old.size + 1
                } else {
                    it.old.size
                }
                if (localScore > score) {
                    score = localScore
                    bestMatch = it
                }

            }
        }
        log.finest("Best match was: ${bestMatch}")
        return bestMatch
    }

    fun translateCons(cons: Consultation, resultText: StringBuilder): List<Phoenix> {
        var oldValue = 0.0
        var newValue = 0.0
        val newVals = LinkedList<Phoenix>()
        fun internalTranslate(ml: MutableList<String>) {
            val rule = findMatchingRule(ArrayList(ml))
            while (!ml.isEmpty()) {
                var matched = false
                val rnd = Math.random()
                var accum = 0.0
                rule.new.forEach { px ->
                    if (!matched) {
                        accum += px.percent
                        if (rnd <= accum) {
                            px.code.split("\\+".toRegex()).forEach { code ->
                                val parts = code.split("\\*".toRegex())
                                var rep = if (parts.size == 2) {
                                    parts[1].toDouble()
                                } else {
                                    1.0
                                }
                                while (rep-- > 0) {
                                    val phoenix = findPhoenix(parts[0])
                                    newVals.add(phoenix)
                                    newValue += (phoenix.AL + phoenix.TL) * tp
                                }
                            }
                            matched = true
                        }
                    }

                }
                // Die Positionen, die von der aktuellen Rule konsumiert wurden,
                // aus der Liste entfernen und die restlichen Positionen erneut matchen.
                if (matched) {
                    rule.old.forEach {
                        if (it.equals("*")) {
                            ml.removeAt(0)
                        } else if (ml.contains(it)) {
                            ml.remove(it)
                        }
                    }
                    if (ml.size > 0) {
                        internalTranslate(ml)
                    }
                }

            }
        }
        internalTranslate(cons.tarmedpos.map { oldValue += it.value; it.id }.toMutableList())
        resultText.append(cons.date).append(": ").append("alt: ${round100(oldValue)}; neu: ${round100(newValue)}\n")
        return newVals

    }

    /**
     * Die Phoenix-Position zu einem Code finden. Wenn der Code noch nicht in phoenixData ist wird er
     * in der Gesamtliste gesucht und eingelesen.
     */
    fun findPhoenix(code: String): Phoenix {
        var ret = phoenixData.get(code)
        if (ret == null) {
            val result = phoenixList.filter { it.getCode() == code }
            if (result.size > 0) {
                phoenixData.put(code, result.get(0))
                ret = result[0]
            } else {
                throw Exception("could not find Phoenix Posotion for ${code}")
            }

        }
        return ret
    }

    data class Transcription(val code: String, val percent: Double)
    data class Rule(val old: List<String>, val new: List<Transcription>)


}

/**
 *  Repräsentation der Tarmedpositionen,wie sie im Phoenix-Browser vorliegen.
 */
class Phoenix(val line: String) {
    val fields by lazy { line.split("|") }
    fun getChapter() = fields[0]

    fun getCode() = fields[1]
    // "teure" Decodierung erst machen, wenn sie wirklich gebraucht wird.
    val AL by lazy { decode(fields[6]) }
    val TL by lazy { decode(fields[7]) }
    fun getLang() = fields[9]

    /**
     * Aus irgendeinem Grund sind einige Felder der Datenbank mit XOR verschleiert
     */
    fun decode(source: String): Double {
        val xormask = "0996099609960996" // whatever
        val result = StringBuilder()
        for (i in 0..source.length - 1 step 2) {
            val s = Integer.parseInt(source.substring(i, i + 2), 16)
            result.append((s xor xormask[i shr 1].toInt()).toChar())
        }
        return result.toString().toDouble()
    }

}

