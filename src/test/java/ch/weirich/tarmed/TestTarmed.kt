/*
 * Copyright (c) 2016 by G. Weirich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package ch.weirich.tarmed

import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import java.io.File

/**
 * Created by gerry on 03.04.16.
 */

class TestClass {

    @Test
    fun testTranscript(){
        val t=Transcript()

    }

    @Test
    fun testTarmed() {
        val resultText=StringBuilder()
        val loader = Loader()
        val dir = File("target/test-classes")
        loader.addDirectory(dir,resultText)
        Assert.assertTrue(1==1)
    }

    @Test
    fun testTranslation() {
        val resultText = StringBuilder()
        val tr = Transcript()
        val dir = File("target/test-classes")
        val bills = Loader().addDirectory(dir, resultText)
        Assert.assertTrue(bills.size > 0)
        val tral1 = tr.translateCons(bills[0], resultText)
        val total1=tral1.fold(0.0){ sum, px ->
           sum+(px.AL+px.TL)
        }
        Assert.assertEquals(total1,51.24,0.05)
        val tral2=tr.translateCons(bills[1],resultText)
        val total2=tral2.fold(0.0){ sum, px ->
            sum+(px.AL+px.TL)
        }
        Assert.assertEquals(total2,17.08,0.05)
        val tral3=tr.translateCons(bills[2],resultText)
        val total3=tral3.fold(0.0){ sum, px ->
            //println(px.getCode())
            sum+(px.AL+px.TL)

        }
        val totalr=(Math.round(total3*100))/100.0
        Assert.assertTrue( (totalr==71.92) || (totalr== 86.31))
        //Assert.assertEquals(total3,71.92,0.05)

    }

    @Test
    fun testDecoder(){
        val px=Phoenix("Test")
        Assert.assertEquals(9.75,0.01,px.decode("09170E0E"))
    }
}