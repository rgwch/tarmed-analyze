Tarmed-Analyzer
===============

Dieses Programm analysiert Arzrechnungen im Schweizer [XML Arztrechnungsformat 4.0](http://www.forum-datenaustausch.ch/xmlstandards/xml_4.0/xml_rechnung_xml_4.0/xml_rechnung_arztrechnung_xml_4.0), 
und prüft die Auswirkung einer Tarifänderung gemäss der von der [FMH](http://www.fmh.ch) und anderen erarbeiteten [Tarmed-Revision](http://www.fmh.ch/ambulante_tarife/revision_ambulanter-tarif.html).
Es nutzt dazu die von der FMH in Form des Phoenix-Browsers bereitgestellten Daten.

Dazu wird jede Rechnung anhand bestimmter konfigurierbarer Annahmen vom alten Tarif in den neuen umgerechnet.


 * [Systemvoraussetzungen und Programmstart](#usage)
 * [Konfiguration](#config)
 * [Grenzen](#limits)
 * [Nutzungsbedingungen](#license)
 * [Beispielkonfiguration](#sample)
 * [Selber machen](#compile)
 
### Systemvoraussetzungen und Programmstart<a name="usage" />
 
 Tarmed-Analyzer läuft auf Macintosh-, Linux- und vermutlich auch Windows-Computern (letzteres nicht getestet). Voraussetzung ist,
  dass [Java](https://www.java.com/de/download/) in einer Version 1.6 oder höher installiert ist. (Die Version, die Sie
  vermutlich ohnehin installiert haben, um den Tarmed-Browser zu verwenden, erfüllt den Zweck).
  
  Wenn java installiert ist, kann das Programm von einer Kommandozeile aus folgendermassen gestartet werden:
  
      java -jar tarmed-analyzer-x.y.z.jar <Rechnungsverzeichnis> <konfigurationsdatei>
      
  (Wobei x.y.z natürlich auf die tatsächliche Versionsnummer im Dateinamen gesetzt werden müssen). 
  
  Mit &lt;Rechnungsverzeichnis&gt; ist ein Verzeichnis gemeint, in dem sich beliebig viele Arztrechnungen im XML-Format
  befinden. Das ist dasselbe Format, in dem Sie Rechnungen beispielsweise ans Trust-Center oder einen Rechnungsintermediär senden.
  
  Die Angabe der &lt;Konfigurationsdatei&gt; kann auch weggelassen werden, dann wird die interne Vorgabe (siehe unten) verwendet.
  
### Konfiguration<a name="config" />
      
 Die Konfiguration der Umrechnung erfolgt über eine Reihe von Regeln, die in einer Konfigurationsdatei eingetragen sind.  
 Dies ist notwendig, weil es einerseits im alten Tarif Positionen gibt, die im neuen wegfallen (z.B. 00.0015), während
 umgekehrt im neuen Tarif auch neue Positionen aufgenommen wurden, die kein Pendant im alten Tarif haben (z.B. AT.3001).
    
 Letzlich lässt sich nicht im Voraus sagen, wie die Ärzte die Tarifrevision umsetzen würden. Daher erlaubt das Programm Ihnen,
    festzulegen, wie Sie selbst den neuen Tarif anwenden würden, und zu berechnen, was das für Auswirkungen hätte.
    
    
Jede Regel ist folgendermassen aufgebaut:
    
      match{
           tarmedposition_alt_1
           tarmedposition_alt_2
           ....
       to
           revidierte_position * Zahl + Zusatzposition * Zahl:Wahrscheinlichkeit | alternative_position*Zahl:Wahrscheinlichkeit
        }
        
und bewirkt folgendes: Wenn in einer Konsultation die oben aufgeführten Positionen gefunden werden, dann wird dafür
eine der unten angeführten, mit | unterteilten Alternativen eingesetzt. Der Term "Wahrscheinlichkeit" wurde deshalb gewählt, weil wahrscheinlich
        nicht alle Konsultationen exakt gleich umgesetzt würden. Eine bisher 10-Minütige Hausarztkonsultation kann beispielsweise
        entweder wieder als 10-Minutige Konsultation verrechnet werden, oder aber, wenn eine problemorientierte Untersuchung
        gemacht wurde, als Grundkonsultation mit kleinem Organstatus. In der Konfiguration kann man schätzen, in wieviel
        Prozent der Fälle welche Umsetzung korrekt wäre. Die Summe aller Wahrscheinlichkeiten in einer Alternativen-Liste muss 1.0 ergeben.
        
Sie können beliebig viele solcher Regel-Blöcke in die Konfigurationsdatei aufnehmen. Wenn auf eine Konsultation mehrere 
     Regeln anwendbar sein sollte, dann wird die Regel mit den meisten passenden Tarmedpositionen gewähl. Wenn mehrere Regeln
     gleich viele passende Positionen hat, dann wird diejenige gewählt, die näher an der Gesamtzahl an Positionen der Konsultation ist. 
    Wenn immer noch mehrere Regeln gleich gut sind, wird die gewählt, die im Regelsatz weiter _oben_ steht.
        
        
Am Ende der Konfigurationsdatei sollte eine Regel stehen, die so aussieht:

    match{
        *
    to
        revidierte_position * zahl
    }
   
Diese Regel wird immer dann eingesetzt, wenn gar keine andere Regel gefunden werden konnte.   

Leerzeilen und Leerstellen können zur Gliederung beliebig eingesetzt werden, sie werden bei der Interpretation nicht 
beachtet. Zeilen, die mit # beginnen, gelten als Kommentar und werden bei der Interpretation ebenfalls übergangen.

Weiter unten finden Sie eine kommentierte Konfigurationsdatei.
        
### Grenzen<a name="limits" />
        
Dieses Programm kann nur eine grobe Abschätzung der Revisionsfolgen liefern. Ausserdem betrachtet es ausschliesslich Tarmed-Positionen.
     andere Leistungen (Labor, Medikamente etc.) werden schlicht weggelassen. Es ist auch nicht möglich, einen Wandel des
      Abrechnungsverhaltens exakt in so einfachen Regeln zu erfassen. Und last but not least gibt es keine Garantie dafür, dass dieses Programm
     fehlerfrei arbeitet.
         
Wenn Sie tarmed-analyzer mehrfach über dieselben Rechnungen laufen lassen, werden Sie unterschiedliche Resultate bekommen. Das
         liegt daran, dass einige Umsetzungen ja mit Wahrscheinlichkeiten arbeiten, die während des Programmlaufs mit einem
         Zufallsgenerator simuliert werden.
         
         
### Nutzungsbedingungen<a name="license" />
         
Sie dürfen dieses Programm nur verwenden, wenn Sie folgenden Bedingungen zustimmen:
         
  * Es wird keinerlei Garantie dafür übernommen, dass das Programm erwartungsgemäss funktioniert, oder überhaupt funktioniert.
  * Das Programm arbeitet mit Annahmen, die falsch sein können. Damit werden auch die Resultate falsch sein.
  * Sie brauchen für dieses Programm nichts zu bezahlen. 
  * Sie dürfen dieses Programm oder Teile davon auch Anderen weitergeben oder zur Verfügung stellen. Sie dürfen dafür aber
  keine Bezahlung verlangen.
  * Sie dürfen dieses Programm beliebig verändern. Sie dürfen aber die original-Copyrightnotizen nicht entfernen, sondern nur
  ergänzen.
  
Fehlermeldungen, Hinweise und Anregungen zu diesem Programm sind jederzeit willkommen.  
  
### Beispiel für eine Konfigurationsdatei<a name="sample" />

Diese Konfiguration wird als Standard verwendet, wenn beim Programmstart keine eigene Konfigurationsdatei angegeben wird

<hr>

<pre>
# Globale Annahmen
global{
    tp: 0.83     # Taxpunktwert
}

# Hausarzt- Konsultation 20 Minuten
# Wird in 20% der Fälle zu AF.3003 (grosser Status)
# In 79% der Fälle zu 4x AF.1002 (Hausärztliche Untersuchung)
# In 1% der Fälle zu 4xAF.1002 plus SZ.5010 (Wechselzeit in besonderen Fällen bei resistenten Erregern, 5 Minuten Totaldesinfektion des Raums)

match{
    00.0010
    00.0015
    00.0020
    00.0020
    00.0030
to
    AT.3003:0.2 | AF.1002 * 4 : 0.79 | AF.1002 * 4 + SZ.5010 : 0.01
}
# Kinderzuschlag
match{
    00.0040
to
    BI.5000: 1.0
}

# Kleine Untersuchung
match{
    00.0410
to
    AT.3002:1.0
}

# Grosse Untersuchung
match{
    00.0420
to
    AT.3003: 1.0
}

#Konsultation 15 Minuten
#Annahme: 10% davon seien dringlich

match{
    00.0010
    00.0015
    00.0020
    00.0030
to
    AT.3002 : 0.2 | AT.3004 * 3 : 0.7 | AT.3002+AF.0070:0.1
}

#Konsultation ohne Zuschlag
match{
  00.0010
  00.0020
  00.0030
to
  AF.0000 *3:1.0
}

#EKG
match{
  17.0010 to FT.0001+SZ.0001
}

#Notfall in Sprechstunde
match{
  00.2510 to AF.0072
}

#Telefon
match{
  00.0110 to AF.0003
}

# Grundleistung Röntgen Arztpraxis
match{
  39.0020
  39.2000
 to
  # Nur noch Befundung und Wechsel?
  PQ.0020+PQ.0010
}

match{
    39.0021  
    39.2000
   to
    PQ.0020+PQ.0010
}

#Rx Thorax
match{
  39.0190 to PQ.1001
}

#Rx Vorfuss/Zehen ap und schräg
match{
    39.0370
    39.0375
  to
    PQ.2022+PQ.2023
}

#Rx HWS
match{
    39.0130
    39.0135
   to
    PQ.0501+PQ.0502
}

#Rx Schulter
match{
    39.0220
    39.0225
  to
    PQ.1501+PQ.1502
}

# Default für vorher nicht erfasste Positionen.
# Dies muss an letzter Stelle stehen

match{
  *
to
    AF.0000 : 1.0
}

</pre>

### Selber machen<a name="compile" />

Wenn Sie das Programm verbessern und/oder selber compilieren wollen, benötigen Sie ein Java Development Kit und 
[Apache Maven](https://maven.apache.org) Version 3.0 oder höher. Ausserdem ist [git](https://git-scm.com) empfehlenswert. 
 
Mit folgenden Schritten erstellen Sie das Programm:

    git clone https://gitlab.com/rgwch/tarmed-analyze.git
    cd tarmed-analyze
    mvn clean package
    
Das tarmed-analyze.jar befindet sich dann im Unterverzeichnis "Target"    
